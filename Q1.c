#include <stdio.h>
  
   
    //Calculating attendence with respective to ticket price
    int attendees(int price){
    int	 attendees = (120- (price-15)/5*20);
    	return attendees;
	}
   //Calculating the revenue of the movie theater
	int  revenue (int price, int attendees  ){
	
	  int revenue= price * attendees ;
	  return revenue;
    }
    //Calculating the cost 
    int cost ( int attendees ){
    	
    	int cost= 500 + (3*attendees );
    	return cost; 
	}
	//Calculating the profit earn
    int profit (int price, int attendees){
    	int r= revenue (price,attendees ); 
    	int c=  cost (attendees );
    	int profit =r-c;
    	return profit;
	}
	//Display the relationship
	void display(){
		int price , p,maxp = 0, maxt = 0;   //maxp = maximum profit, maxt = maximum ticket price
		  
		printf("Price\t Attendees\t Profit\n");
		  	
		for (price=5; price<=35; price+=5) {
	         p = profit(price,attendees(price));
		
			if(maxp < p){
		    	maxp = p;
		      	maxt = price;
		}
			printf("Rs.%d \t   %d\t     Rs.%d\n",price ,attendees(price),p );
		}
		printf("\n Above table shows the relationship between ticket price and profit");
	  printf("\nThe highest profit is Rs.%d \n Highest profit can earn by under Rs.%d price",maxp,maxt);
	
	}

int main (){
  

	 display();
        return 0;
}
